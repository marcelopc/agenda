﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace Agenda2._0
{
    class DALContato
    {
        private DbAccess objConexao;

        public DALContato(DbAccess conexao)
        {
            objConexao = conexao;
        }

        public void Incluir(Contato contato)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = objConexao.Conexao;
            cmd.CommandText = "insert into contato(con_nome, con_email, con_fone, con_rua, con_bairro, con_cidade ,con_estado, con_cep) " +
                "values (@nome, @email, @fone, @rua, @bairro, @cidade, @estado, @cep); select @@identity";

            cmd.Parameters.AddWithValue("@nome", contato.Nome);
            cmd.Parameters.AddWithValue("@email", contato.Email);
            cmd.Parameters.AddWithValue("@fone", contato.Fone);
            cmd.Parameters.AddWithValue("@rua", contato.Rua);
            cmd.Parameters.AddWithValue("@bairro", contato.Bairro);
            cmd.Parameters.AddWithValue("@cidade", contato.Cidade);
            cmd.Parameters.AddWithValue("@estado", contato.Estado);
            cmd.Parameters.AddWithValue("@cep", contato.Cep);
            objConexao.Conectar();
            contato.Codigo = Convert.ToInt32(cmd.ExecuteScalar());
            objConexao.Desconectar();





        }
        public void Alterar(Contato contato)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = objConexao.Conexao;
            cmd.CommandText = "update contato set con_nome = @nome, con_email = @email, con_fone = @fone, con_rua = @rua, " +
                "con_bairro = @bairro, con_cidade = @cidade, con_estado = @estado, con_cep = @cep where con_cod = @codigo";

            cmd.Parameters.AddWithValue("@codigo", contato.Codigo);
            cmd.Parameters.AddWithValue("@nome", contato.Nome);
            cmd.Parameters.AddWithValue("@email", contato.Email);
            cmd.Parameters.AddWithValue("@fone", contato.Fone);
            cmd.Parameters.AddWithValue("@rua", contato.Rua);
            cmd.Parameters.AddWithValue("@bairro", contato.Bairro);
            cmd.Parameters.AddWithValue("@cidade", contato.Cidade);
            cmd.Parameters.AddWithValue("@estado", contato.Estado);
            cmd.Parameters.AddWithValue("@cep", contato.Cep);
            objConexao.Conectar();
            cmd.ExecuteNonQuery();
            objConexao.Desconectar();
        }
        public void Excluir(int codigo)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = objConexao.Conexao;
            cmd.CommandText = "delete from contato where con_cod = @codigo";

            cmd.Parameters.AddWithValue("@codigo", codigo);

            objConexao.Conectar();
            cmd.ExecuteNonQuery();
            objConexao.Desconectar();
        }
        public DataTable Localizar(string valor)
        {
            DataTable tabela = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter("select con_cod as 'Código', con_nome as Nome, con_email as Email, con_fone as Fone, con_rua as Rua, " +
                "con_bairro as Bairro, con_cidade as Cidade, con_estado as Estado, con_cep as CEP " +
                "from contato where con_nome like '%" + valor + "%'", objConexao.StringConexao);
            da.Fill(tabela);
            return tabela;
        }
        public Contato CarregaContato(int codigo)
        {
            Contato modelo = new Contato();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = objConexao.Conexao;
            cmd.CommandText = "select con_cod, con_nome, con_email, con_fone, con_rua, " +
                "con_bairro, con_cidade, con_estado, con_cep from contato where con_cod =" + codigo;
            objConexao.Conectar();
            SqlDataReader registro = cmd.ExecuteReader();
            if(registro.HasRows)
            {
                registro.Read();
                modelo.Codigo = Convert.ToInt32(registro["con_cod"]);
                modelo.Nome = Convert.ToString(registro["con_nome"]);
                modelo.Email = Convert.ToString(registro["con_email"]);
                modelo.Fone = Convert.ToString(registro["con_fone"]);
                modelo.Rua = Convert.ToString(registro["con_rua"]);
                modelo.Bairro = Convert.ToString(registro["con_bairro"]);
                modelo.Cidade = Convert.ToString(registro["con_cidade"]);
                modelo.Estado = Convert.ToString(registro["con_estado"]);
                modelo.Cep = Convert.ToString(registro["con_cep"]);
            }
            return modelo;
        }

        public List<string> listaEstado()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = objConexao.Conexao;
            cmd.CommandText = "select Uf from Estado";
            objConexao.Conectar();

            List<string> uf = new List<string>();

                    SqlDataReader dr = cmd.ExecuteReader();

                    while (dr.Read())
                    {
                        uf.Add(dr["Uf"].ToString());
                    }
            objConexao.Desconectar();

            return uf;
            
        }
        public List<string> ListaCidade(string uf)
        {
            
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = objConexao.Conexao;
            cmd.CommandText = "select Nome from Municipio where Uf = @Uf";
            objConexao.Conectar();

            List<string> cidade = new List<string>();

           
            cmd.Parameters.AddWithValue("@Uf", uf);
            SqlDataReader dr = cmd.ExecuteReader();

           while (dr.Read())
           {
             cidade.Add(dr["Nome"].ToString());
           }

            objConexao.Desconectar();

            return cidade;

        }


    }
}
