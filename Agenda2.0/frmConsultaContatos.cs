﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Agenda2._0
{
    public partial class frmConsultaContatos : Form
    {
        public string strConexao = @"Data Source=MARCELO-PC\sqlexpress;Initial Catalog=Agenda;Persist Security Info=True;User ID=desenvolvedor;Password=123456";
        public int codigo = 0;

        public frmConsultaContatos()
        {
            InitializeComponent();
        }

        private void btLocalizar_Click(object sender, EventArgs e)
        {
            DbAccess cx = new DbAccess(strConexao);
            DALContato dal = new DALContato(cx);

            dgDados.DataSource = dal.Localizar(tbNomedoContato.Text);
        }

        private void dgDados_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex >= 0)
            {
                this.codigo = Convert.ToInt32(dgDados.Rows[e.RowIndex].Cells[0].Value);
                this.Close();
            }
        }
    }
}
