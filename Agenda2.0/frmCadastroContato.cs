﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Agenda2._0
{
    public partial class frmCadastroContato : Form
    {
        public string operacao = "";

        public string strConexao = @"Data Source=MARCELO-PC\sqlexpress;Initial Catalog=Agenda;Persist Security Info=True;User ID=desenvolvedor;Password=123456";

        public frmCadastroContato()
        {
            InitializeComponent();

            DbAccess conexao = new DbAccess(strConexao);
            DALContato dal = new DALContato(conexao);
            cbEstado.DataSource = dal.listaEstado();
            cbEstado.DisplayMember = "UF";
        }

        public void AlteraBotoes(int op)
        {
            pDados.Enabled = false;
            btnInserir.Enabled = false;
            btnLocalizar.Enabled = false;
            btnSalvar.Enabled = false;
            btnExcluir.Enabled = false;
            btnAlterar.Enabled = false;
            btnCancelar.Enabled = false;

            switch (op)
            {
                case 1:
                    btnInserir.Enabled = true;
                    btnLocalizar.Enabled = true;
                    break;
                case 2:
                    pDados.Enabled = true;
                    btnSalvar.Enabled = true;
                    btnCancelar.Enabled = true;
                    break;
                case 3:
                    btnExcluir.Enabled = true;
                    btnAlterar.Enabled = true;
                    btnCancelar.Enabled = true;
                    break;
            }
            
        }
        public void LimpaCampos()
        {
            tbCodigo.Clear();
            tbNome.Clear();
            tbEmail.Clear();
            tbFone.Clear();
            tbCep.Clear();
            tbRua.Clear();
            tbBairro.Clear();

        }

        private void frmCadastroContato_Load(object sender, EventArgs e)
        {
            this.AlteraBotoes(1);
        }

        private void btnInserir_Click(object sender, EventArgs e)
        {
            this.operacao = "inserir";
            this.AlteraBotoes(2);
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.AlteraBotoes(1);
            this.LimpaCampos();
            
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {

            try
            {
                if(tbNome.Text.Length <= 0)
                {
                    MessageBox.Show("Nome obrigatório");
                    return;
                }
                Contato contato = new Contato();
                contato.Nome = tbNome.Text;
                contato.Email = tbEmail.Text;
                contato.Fone = tbFone.Text;
                contato.Cep = tbCep.Text;
                contato.Rua = tbRua.Text;
                contato.Bairro = tbBairro.Text;
                contato.Cidade = cbCidade.Text;
                contato.Estado = cbEstado.Text;

                DbAccess conexao = new DbAccess(strConexao);
                DALContato dal = new DALContato(conexao);
                if (operacao == "inserir")
                {

                    try
                    {
                        dal.Incluir(contato);
                        MessageBox.Show("O código gerado foi: " + contato.Codigo.ToString());
                    }
                    catch (Exception erro)
                    {
                        MessageBox.Show(erro.Message);
                    }
                }
                else
                {
                    contato.Codigo = Convert.ToInt32(tbCodigo.Text);
                    dal.Alterar(contato);
                    MessageBox.Show("Contato Alterado");

                }
                this.AlteraBotoes(1);
                this.LimpaCampos();
            }
            catch(Exception erro)
            {
                MessageBox.Show(erro.Message);
            }
            
        }

        private void btnLocalizar_Click(object sender, EventArgs e)
        {
            frmConsultaContatos consulta = new frmConsultaContatos();
            consulta.ShowDialog();

            if (consulta.codigo != 0)
            {
                DbAccess conexao = new DbAccess(strConexao);
                DALContato dal = new DALContato(conexao);
                Contato contato = dal.CarregaContato(consulta.codigo);

                tbCodigo.Text = contato.Codigo.ToString();
                tbNome.Text = contato.Nome.ToString();
                tbEmail.Text = contato.Email.ToString();
                tbFone.Text = contato.Fone.ToString();
                tbCep.Text = contato.Cep.ToString();
                tbRua.Text = contato.Rua.ToString();
                tbBairro.Text = contato.Bairro.ToString();
                cbCidade.Text = contato.Cidade.ToString();
                cbEstado.Text =contato.Estado.ToString();
                this.AlteraBotoes(3);
            }
            consulta.Dispose();
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            this.operacao = "alterar";
            this.AlteraBotoes(2);
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            DialogResult d = MessageBox.Show("Deseja excluir o registro?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if(d.ToString() == "Yes")
            {
                DbAccess conexao = new DbAccess(strConexao);
                DALContato dal = new DALContato(conexao);

                dal.Excluir(Convert.ToInt32(tbCodigo.Text));
                MessageBox.Show("Contato Excluido");

                this.AlteraBotoes(1);
                this.LimpaCampos();

            }
        }

        private void cbEstado_SelectedIndexChanged(object sender, EventArgs e)
        {
            DbAccess conexao = new DbAccess(strConexao);
            DALContato dal = new DALContato(conexao);
            cbCidade.DataSource = dal.ListaCidade(cbEstado.Text);
            cbCidade.DisplayMember = "Cidade";
            
        }
    }
}
