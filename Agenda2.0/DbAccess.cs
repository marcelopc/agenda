﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Agenda2._0
{
    class DbAccess
    {
        private String _stringConexao;
        private SqlConnection _conexao;

        public DbAccess(string dadosConexao)
        {
            this._conexao = new SqlConnection();
            this.StringConexao = dadosConexao;
            this._conexao.ConnectionString = dadosConexao;
        }

        public string StringConexao
        {
            get { return this._stringConexao; }
            set { this._stringConexao = value; }
        }

        public SqlConnection Conexao
        {
            get { return this._conexao; }
            set { this._conexao = value; }
        }

        public void Conectar() { this._conexao.Open(); }
        public void Desconectar() { this._conexao.Close(); }
    }
}
